<?php
/*
+-------------------------------------------------------------------------+
| Copyright (C) 2002 Igor Genibel http://genibel.org/                     |
| Copyright (C) 2005--2007 Christoph Berg <myon@debian.org>               |
|                                                                         |
| This program is free software; you can redistribute it and/or           |
| modify it under the terms of the GNU General Public License             |
| as published by the Free Software Foundation; either version 2          |
| of the License, or (at your option) any later version.                  |
|                                                                         |
| This program is distributed in the hope that it will be useful,         |
| but WITHOUT ANY WARRANTY; without even the implied warranty of          |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           |
| GNU General Public License for more details.                            |
+-------------------------------------------------------------------------+
*/


/*
    Print the footer information such as page generating time.
*/
function print_footer()
{
    global $time;
    global $prefix;
    $fp = fopen("$prefix/extract.date", "r");
    $general = rtrim(fgets($fp));
    fclose($fp);
    $fp = fopen("$prefix/projectb.date", "r");
    $projectb = rtrim(fgets($fp));
    fclose($fp);
    $fp = fopen("$prefix/bugs.date", "r");
    $bugs = rtrim(fgets($fp));
    fclose($fp);

    $footer = "Updated: general information: $general, projectb: $projectb, bugs: $bugs. ";
    list($micro,$sec) = explode(' ', microtime());
    list($smicro,$ssec) = explode(' ', $time);
    $footer .= sprintf("Time needed to generate page: %.2fs", $sec-$ssec+$micro-$smicro) . html_br();
    $footer .= '<a href="https://salsa.debian.org/qa/qa">Git repository</a> | ';
    $footer .= "Report problems with DDPO to the ". html_a("qa.debian.org pseudopackage", "https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=qa.debian.org") .".";

    print html_p(html_small($footer));
}

/*
    form function 
    Args:
        $method: "GET" "POST"
        $preamble: inner form data not considered as visible
                   parameters
        $data: visible parameters
        $name: submit button name
    Returns:
        an valid html form
*/
function html_form($action, $method, $preamble, $fields, $name)
{
    $form = "<form method=\"$method\" action=\"$action\">";
    $form .= $preamble;
    $form .= $fields;
    $form .= html_input_submit($name);
    $form .= "</form>\n";
    return $form;
}

/*
    label function
    Args:
        $text: text to be labelized
    Return:
        labelized text
*/
function html_label($text)
{
    return "<label>$text</label>\n";
}

/*
    submit button function
    Args:
        $name: submit button name
    Returns:
        a valid html submit button
*/
function html_input_submit($name)
{
    return "<input type=\"submit\" value=\"$name\">\n";
}

/*
    text field unction
    Args:
        $name: the text field name
    Returns: 
        a valid html text field 
*/
function html_input_text($name, $value = "")
{
    if ($value) $value = " value=\"$value\"";
    return "<input type=\"text\" size=\"30\" maxlength=\"80\" name=\"$name\"$value>\n";
}

/*
    hidden field unction
    Args:
        $name: the hidden field name
        $value: hidden field value
    Returns: 
        a valid html hidden field 
*/
function html_input_hidden($name,$value)
{
   return "<input type=\"hidden\" name=\"$name\" value=\"$value\">\n"; 
}

/*
    radio button function
    Args:
        $name: the radio button field name
        $value: radio button field value
        $checked: boolean 
        $text: text to explain the radio button
    Returns: 
        a valid html radio button
*/
function html_input_radio($name,$value,$checked,$text)
{
    $data = "<input type=\"radio\" name=\"$name\" value=\"$value\" id=\"$name-$value\"";
    if($checked) $data .= " checked";
    $data .= "><label for=\"$name-$value\">$text</label>";
    return "$data\n";
}

/*
    table header element
    Args:
        $title: header text 
        $extra: th args (align, ...)
        $rowspan: how many rows in the table the header covers
        $colspan: how many cols in the table the header covers
    Returns: 
        A valid table header element
*/
function html_th($title, $extra = "", $rowspan = "", $colspan = "")
{
    if($colspan == "") $colspan = 1;
    if($rowspan == "") $rowspan = 1;
    if ($extra != "")
    {
        return "<th rowspan=\"$rowspan\" colspan=\"$colspan\">" . html_color("$title ", "green") . html_br() . html_color("($extra)", "black") . "</th>\n";
    }else{
        return "<th rowspan=\"$rowspan\" colspan=\"$colspan\">" . html_color($title, "green") . "</th>\n";
    }
}

/*
    table element
    Args:
        $data: text element
        $width: width in %
        $rowspan: how many rows in the table the element covers
        $colspan: how many cols in the table the element covers
    Returns:
        A valid table element
*/
function html_td($data, $width = "", $colspan = "", $rowspan = "", $sorttable_customkey = "")
{
    if (!empty($width)) $width = " width=\"".$width."\"";
    if (!empty($rowspan)) $rowspan = " rowspan=\"".$rowspan."\"";
    if (!empty($colspan)) $colspan = " colspan=\"".$colspan."\"";
    if (!empty($sorttable_customkey)) $sorttable_customkey = " sorttable_customkey=\"".$sorttable_customkey."\"";
    return "<td".$width.$rowspan.$colspan.$sorttable_customkey.">".$data."</td>\n";
}

/*
    table line
    Args:
        $data: all tds
        $align: center, left, right
        $bgcolor: background color
    Returns:
        A valid table line
*/
function html_tr($data, $align = "", $bgcolor = "")
{
    if($align == "") $align = "center";
    if($bgcolor == "")
    {
        return "<tr align=\"$align\">$data</tr>\n";
    }else{
        return "<tr align=\"$align\" bgcolor=\"$bgcolor\">$data</tr>\n";
    }
}

/*
    table
    Args:
        $header: all table headers (a tr)
        $data: array of data lines
        $width: table width in %
    Returns:
        A valid html table
*/

function html_table_begin($header, $width, $class)
{
    if($width) $width = " width=\"${width}%\"";
    if($class) $class = " class=\"$class\"";
    $table = "<table$width$class border=\"1\" cellpadding=\"3\" cellspacing=\"1\" summary=\"\">";
    $table .= $header ;
    return $table;
}

function html_table_end()
{
    return "\n</table>\n";
}

function html_table($header, $data, $width, $class)
{
    $table = html_table_begin($header, $width, $class);
    foreach ($data as $line) $table .= $line; 
    return "$table".html_table_end();
}

/*
    Html Anchor
    Args:
        $text: anchor text
        $link: anchor link
        $class: anchor class
        $title: info bullet text
    Returns:
        A valid html anchor
*/
function html_a($text, $link, $class = "", $title = "", $name = "")
{
    if (!empty($title)) $title = " title=\"$title\"";
    if (!empty($class)) $class = " class=\"$class\"";
    if (!empty($name)) $name = " name=\"$name\"";
    return "<a$class$name href=\"$link\"$title>$text</a>";
}
function html_a_name($text, $name)
{
    return "<a name=\"$name\">$text</a>";
}

/*
    Html headers
    Args: 
        $text: header text
        $level: header level
        $align: header align
    Returns: 
        A valid html header 
*/
function html_h($text, $level, $align = "")
{
    if ($align == "")
    {
        return "<h$level>$text</h$level>\n";
    }else{
        return "<h$level align=\"$align\">$text</h$level>\n";
    }
}

/* The following function are basic html functions (from html_tt to html_span */
function html_tt($text)
{
    return "<tt>$text</tt>";
}

function html_b($text)
{
    return "<b>$text</b>";
}

function html_u($text)
{
    return "<u>$text</u>";
}

function html_em($text)
{
    return "<em>$text</em>";
}

function html_p($text)
{
    return "<p>$text</p>";
}


function  html_blank()
{
    return "&nbsp;";
}

function html_br()
{
    return "<br>\n";
}

function html_hr()
{
    return "<hr>\n";
}

function html_small($text)
{
    return "<small>$text</small>";
}

function html_font($text, $data)
{
    return "<font $data>$text</font>";
}

function html_color($text, $color)
{
    return "<font color=\"$color\">$text</font>";
}

function html_div($text,$id="",$class="",$extra="")
{
    if ($id) $id = " id=\"".$id."\"";
    if ($class) $class = " class=\"".$class."\"";
    return "<div".$id.$class." ".$extra.">".$text."</div>\n";
}

function html_span($text,$id="",$class="",$extra="")
{
    if ($id) $id = " id=\"".$id."\"";
    if ($class) $class = " class=\"".$class."\"";
    return "<span".$id.$class." ".$extra.">".$text."</span>";
}

function html_tooltip($text,$title)
{
    return "<span class=\"tooltip\" title=\"".$title."\">".$text."</span>";
}

function html_ul($text, $class = "")
{
    if ($class) $class = " class=\"$class\"";
    return "<ul$class>$text</ul>";
}

function html_li($text)
{
    return "<li>$text</li>";
}

function html_wrap2lines($text)
{
    $list = explode(" ", $text);
    if(count($list) < 2)
    {
	return $text;
    }
    $half = (int)(count($list)/2);
    return implode(html_blank(), array_slice($list, 0, $half)) . " " .
           implode(html_blank(), array_slice($list, $half));
}


/* 
    Function needed to know if the columns has to be displayed or not
*/
function isdisplayed($value)
{
  global $arg_cookie;
  return $arg_cookie[$value] ?? false;
}

/*
    return hidden fields with action parameters to be provided for the form validation
*/
function setaction()
{
    $action = "";
    foreach(array_keys($_GET) as $key) 
    {
        if (($key == 'login') or ($key == 'package') or ($key == 'gpg_key'))
        {
            $action .= html_input_hidden($key,htmlspecialchars($_GET[$key]));
        }
    }
    return $action;
}

?>

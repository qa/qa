#!/usr/bin/python3

# Check the integrity of the MIA database
# Copyright (C) 2003, 2004, 2005  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import mailbox, os, re, stat, string, sys, time
import apt_pkg, status, utils

def help():
    print("""Usage: check-integrity
Check the integrity of the MIA database

Options:
  -h, --help                Show this help and exit
  -f, --fix                 Fix certain problems
""")
    sys.exit(1)


def parse_args():
    global files
    global Cnf, Options

    apt_pkg.init()
    Cnf = apt_pkg.Configuration()
    apt_pkg.read_config_file_isc(Cnf, status.config)

    Arguments = [("h", "help", "Integrity::Options::Help"),
                 ("f", "fix", "Integrity::Options::Fix")]
    for i in ["Help", "Fix"]:
        if not Cnf.exists("Integrity::Options::%s" % i):
            Cnf["Integrity::Options::%s" % i] = ""

    apt_pkg.parse_commandline(Cnf, Arguments, sys.argv)
    Options = Cnf.subtree("Integrity::Options")

    if Options["Help"]:
        help()


def main():
    files = []
    try:
        files = os.listdir(Cnf["Dir::Database"])
    except OSError:
        print("Cannot find directory of database: %s" % path)
        sys.exit(1)
    # Check if any .summary files are missing or .summary files exist for
    # main files which don't exist.
    for file in files:
        root, ext = os.path.splitext(file)
        if ext == ".summary":
            if root not in files:
                utils.warn("Summary file exists for '%s' but main file missing." % root)
        else:
            if (file + ".summary") not in files:
                utils.warn("Summary file for '%s' missing." % root)
                files.remove(file)
        # Check if a file is empty.
        if os.stat(Cnf["Dir::Database"] + "/" + file)[stat.ST_SIZE] == 0:
            utils.warn("File '%s' is empty." % file)

    status.read_status(status.find_status())
    all_status = status.get_all_files()
    # Check for messages which have not been summarized yet and also for
    # summaries for non-existing messages.
    for file in [x for x in files if not x.endswith(".summary")]:
        #print(file)
        try:
            fp = open(Cnf["Dir::Database"] + "/" + file, "r")
        except OSError:
            print("Cannot open file %s" % file)
            sys.exit(1)
        # Don't use status.read_status() to parse the summary file because
        # we want to check whether there are multiple entries for one date.
        info = {}
        unsorted_when = []
        try:
            summary = open(Cnf["Dir::Database"] + "/" + file + ".summary", "r")
        except OSError:
            print("Cannot open file %s" % (file + ".summary"))
            sys.exit(1)
        for line in summary.readlines():
            line = string.rstrip(line)
            result = status.re_summary_line.search(line)
            if result:
                when, tag = result.groups()
                when = int(when)
                if not info.has_key(when):
                    info[when] = tag
                    unsorted_when.append(when)
                else:
                    utils.warn("Key '%d' exists already in '%s' ('%s' vs '%s')." % (when, file, info[when], tag))
            else:
                utils.warn("Cannot parse summary line '%s' for '%s'" % (line, file))
                break
        summary.close()
        sorted_when = info.keys()
        sorted_when.sort()
        if Options["Fix"] and sorted_when != unsorted_when:
            utils.warn("Sorting .summary file of '%s'" % file)
            try:
                summary = open(Cnf["Dir::Database"] + "/" + file + ".summary", "w")
            except OSError:
                print("Cannot open file %s" % (file + ".summary"))
                sys.exit(1)
            for when in sorted_when:
                summary.write("%d: %s\n" % (when, info[when]))
        summary.close()
        mbox = mailbox.UnixMailbox(fp)
        msg = mbox.next()
        while msg:
            date_tz = msg.getdate_tz("Date")
            # subtract the timezone so we get a TZ independent value
            when = int(time.mktime(date_tz[:9])) - date_tz[9] - time.timezone
            # If it's not in info, we didn't summarize the message yet.
            if when not in info.keys():
                utils.warn("File '%s' has an unsummarized message dated %s" % (file, msg.get("Date")))
            else:
                del info[when]
            msg = mbox.next()
        fp.close()

        # info should be empty by now.  If it isn't, we have a summary tag
        # for a message which doesn't exist.
        for when in info.keys():
            utils.warn("File '%s' has summary tag (%s) for non-existing message." % (file, info[when]))


# main()
parse_args()
main()

# vim: ts=4:expandtab:shiftwidth=4:

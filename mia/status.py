# The module which forms the heart of the MIA scripts
# Copyright (C) 2001, 2002, 2003, 2004  Martin Michlmayr <tbm@cyrius.com>
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
# Copyright (C) 2006  Christoph Berg <myon@debian.org>
# Copyright (C) 2024  Mattia Allegro <m.allegro@shadmod.it>
# $Id: status.py 3545 2016-07-20 13:20:16Z mattia $

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# This is a collection of software to track down developers who are
# Missing In Action (MIA).

import os
import pwd
import re
import sys
import time
from pathlib import Path

import apt_pkg

sys.path.extend("/srv/qa.debian.org/carnivore")
import carnivore
from carnivore.carnivore import get as carnivore_get
from carnivore.carnivore import search as carnivore_search

now = time.time()

DEBUG = os.getenv("DEBUG")
PATH_CONFIG = (
    "/srv/qa.debian.org/mia/mia.conf"
    if not DEBUG
    else str(Path(__file__).parent / "mia.conf")
)


config = PATH_CONFIG


re_summary_line = re.compile(r"(\d+)( [a-z]+ [^ \t]+)*:\s*(.*)")

apt_pkg.init()
db_cnf = apt_pkg.Configuration()
apt_pkg.read_config_file_isc(db_cnf, config)
info = {}


def find_status(path=db_cnf["Dir::Database"]):
    files = []

    try:
        for file in os.listdir(path):
            root, ext = os.path.splitext(file)
            if ext == ".summary":
                files.append(root)
    except OSError:
        print("Cannot find directory of database: %s" % path)
        sys.exit(1)

    return files


def read_history(id):
    global info
    id = id.replace("@", "=")

    if id in info:
        return info[id]

    try:
        summary = open(db_cnf["Dir::Database"] + "/" + id + ".summary", "r")
    except IOError:
        return None

    res = {}

    for line in summary.readlines():
        result = re_summary_line.search(line)

        if result is None:
            print("Error parsing file %s: %s" % (summary, line), file=sys.stderr)
            sys.exit(1)

        text = result.group(3)
        # TODO: return sensibly
        if result.group(2):
            extra = result.group(2).strip().split()
            while extra:
                if extra[0] == "nomail":
                    if text.find(";") == -1:
                        text += ";"
                    extra.pop(0)
                    text += " {command-line supplied by %s}" % extra.pop(0)
                elif extra[0] == "from":
                    if text.find(";") == -1:
                        text += ";"
                    extra.pop(0)
                    text += " {from %s}" % extra.pop(0)
                else:
                    print(
                        "Error parsing extra info in %s: %s" % (summary, line),
                        file=sys.stderr,
                    )
                    sys.exit(1)

        t = int(result.group(1))
        while t in res:  # gross hack to make keys unique
            t += 1
        res[t] = text

    summary.close()

    info[id] = res
    return res


regular_statusses = {
    "none": 0,
    "ok": 0,
    "busy": 1,
    "inactive": 2,
    "unresponsive": 3,
    "retiring": 4,
    "mia": 6,
    "needs-wat": 7,
    "retired": 10,
    "removed": 10,
    "role": 99,
}
suspend_statusses = {"ok": -1, "npa": 365 * 24 * 3600, "willfix": 4 * 7 * 24 * 3600}
prod_types = ["nice", "prod", "last-warning", "wat"]


def parse_status(info):
    status = "none"
    status_time = -1
    suspend_status = None
    suspend_until = -1
    prod = "none"
    prod_time = -1
    times = list(info.keys())
    times.sort()
    # too lazy to rename "time" to something else
    for time in times:  # noqa: F402
        value = info[time].split(";")[0]
        for v in value.split(","):
            v = v.strip().lower()
            foo = v.split(None, 1)
            try:
                word = foo[0]
            except IndexError:
                word = ""
            rest = None
            if len(foo) > 1:
                rest = foo[1]
            if v in regular_statusses.keys() and v != "none":
                if (
                    regular_statusses[v] == 0
                    or regular_statusses[v] > regular_statusses[status]
                ):
                    status = v
                    status_time = time
            elif word in suspend_statusses.keys():
                suspend_status = word
                suspend_until = time + parse_for(suspend_statusses[word], rest)
            elif word in prod_types:
                prod = word
                prod_time = time
                # suspend current status by default for 3 weeks in case of
                # outgoing mail, special case: 2 weeks for nice since nice is
                # to be sent twice anyways
                suspend_status = "mailed"
                if prod == "nice":
                    suspend_until = time + parse_for(2 * 7 * 24 * 3600, rest)
                else:
                    suspend_until = time + parse_for(3 * 7 * 24 * 3600, rest)
            elif word in ["in", "out"]:
                # suspend current status by default for 2 weeks in case of
                # mail contact
                susp = time + parse_for(2 * 7 * 24 * 3600, rest)
                if susp > suspend_until:
                    suspend_status = "mailed"
                    suspend_until = susp
            elif v == "-":
                pass
            else:
                # too much stuff yet
                # sys.stderr.write("Warning: parse error in %s\n" % v)
                if not info[time].endswith(" {parse error}"):
                    if info[time].find(";") == -1:
                        info[time] += ";"
                    info[time] += " {parse error}"
                if status == "none":
                    status = "busy"
                    status_time = time

    suspend = ""
    res = {"status": status, "prod": prod}
    if status != "none":
        res["status_time"] = status_time
    if prod != "none":
        res["prod_time"] = prod_time
    if regular_statusses[status] and suspend_status:
        if suspend_until > now:
            res["suspend"] = suspend_status
            res["suspend_until"] = suspend_until
            suspend = " (but %s for another %s)" % (
                suspend_status,
                time_passed(suspend_until, -1),
            )
        else:
            suspend = " (was %s until %s ago)" % (
                suspend_status,
                time_passed(suspend_until),
            )

    prod_for = ""
    if prod_time >= 0:
        prod_for = " for %s" % time_passed(prod_time)
    res["human"] = "Status is %s for %s%s; Prod-level is %s%s" % (
        status,
        time_passed(status_time),
        suspend,
        prod,
        prod_for,
    )
    if not info:
        res["human"] = "Not in database"

    return res


time_spans = {
    "d": 24 * 3600,
    "w": 7 * 24 * 3600,
    "m": 30 * 24 * 3600,
    "y": 365 * 24 * 3600,
}


def parse_for(default, arg):
    if arg is None:
        return default
    re_time = re.compile(r"(\d+)([dwmy])")
    res = 0
    for t in re_time.finditer(arg):
        res += int(t.group(1)) * time_spans[t.group(2)]
    return res


def time_passed(t, multiplier=1):
    return "%sd" % int(multiplier * (now - t) / 24 / 3600)


def write_status(file, id, summary, withMail=1, sender=None):
    try:
        status = open(db_cnf["Dir::Database"] + "/" + file + ".summary", "a")
    except IOError:
        print("Cannot open summary file to write!")
        sys.exit(1)

    nomail = ""
    if not withMail:
        nomail = " nomail %s" % pwd.getpwuid(os.getuid())[0]
    sender_str = ""
    re_sender = re.compile(r"<(.+@.+)>")
    if sender:
        result = re_sender.search(sender)
        if result:
            sender_str = " from %s" % result.group(1)

    string = str(int(id)) + nomail + sender_str + ": " + summary + "\n"
    status.write(string)
    status.close()

    if file in info:
        del info[file]


def parse_history(history):
    lines = []
    all_contacts = list(history.keys())
    all_contacts.sort()
    for when in all_contacts:
        lines.append("    %s: %s" % (get_time(when, "%Y-%m-%d"), history[when]))
    return lines


def get_time(when, format):
    return time.strftime(format, time.gmtime(when))


def searchCarnivore(query):
    return carnivore_search(query)


def get(carnivoreId):
    return miaEntry(carnivore_get(carnivoreId))


class miaEntry:
    def __init__(self, carnivore):
        self.ce = carnivore
        self.id = carnivore.id

    def __repr__(self):
        return "miaEntry(" + (self.ce.ldap + self.ce.email)[0] + ")"

    def getHistory(self):
        mia_db = {}
        role = False
        for id in self.ce.ldap + self.ce.email:
            mia_status = read_history(id)
            if id.find("@lists.") >= 0:
                role = True
            if mia_status and mia_db:
                sys.stderr.write(
                    "Maintainer has duplicate entries in MIA db: %s and %s\n"
                    % (self.mia_path_id, id)
                )
            if mia_status:
                mia_db = mia_status
                self.mia_path_id = id
        # we now think that role addresses with mia process open should not be a
        # reason to fail
        #        if role and mia_db:
        #            sys.stderr.write("Looks like role but has MIA entry: %s \n"
        #                % self.mia_path_id)
        #            sys.exit(1)
        if role:
            mia_db.update({0: "role; Detected mailinglist address"})
        return mia_db

    def getStatus(self):
        return parse_status(self.getHistory())

    def statusIsAtLeast(self, status):
        return (
            regular_statusses[self.getStatus()["status"]] >= regular_statusses[status]
        )

    def getPrettyprintedText(self):
        text = self.ce.getPrettyprintedText()

        history = self.getHistory()
        text += "X-MIA: " + parse_status(history)["human"] + "\n"
        text += "\n".join(parse_history(history))

        return text

    def prettyPrint(self):
        print(self.getPrettyprintedText() + "\n")


# vim: ts=4:expandtab:shiftwidth=4:

#!/usr/bin/python3

# Command-line add an entry to MIA database
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from __future__ import print_function

import os, sys, time, re
import apt_pkg, utils, status

os.umask(0o002)

apt_pkg.init()
Cnf = apt_pkg.Configuration()
apt_pkg.read_config_file_isc(Cnf, status.config)

try:
    maint = sys.argv[1]
except IndexError:
    print("No maintainer given, pass login or email as parameter", file=sys.stderr)
    sys.exit(1)

query = "ldap:"+maint
if maint.find("@") >= 0:
    query = "email:"+maint

# make sure to not allow shell stuff, /'s, etc!
if re.compile(r"[^a-z0-9@+._-]").search(maint, re.I):
    print("Invalid characters found in maintainer, aborting", file=sys.stderr)
    sys.exit(1)

entry = status.searchCarnivore(query)
if not entry or len(entry) != 1:
    print("Couldn't find %s, aborting\n" % maint, file=sys.stderr)
    sys.exit(1)

entry = entry[0]
status.get(entry).prettyPrint()

# TODO: if another identify is in use for this maintainer, use that instead
print("Please supply information to store for the above maintainer:")
status.write_status(maint.replace("@", "="), time.time(), raw_input(), False)
print()
print("Updated. New status:")
print()
status.get(entry).prettyPrint()

# vim: ts=4:expandtab:shiftwidth=4:

#!/usr/bin/env bash
set -eu

KEYRINGS_DIR=/srv/qa.debian.org/data/keyrings
if ! [ -f "${KEYRINGS_DIR}/README"  ]; then
    mkdir -p "$KEYRINGS_DIR"
    /srv/qa.debian.org/data/cronjobs/keyrings
fi

SOURCES_DIR=/srv/qa.debian.org/ftp/debian/dists/unstable/source/source
mkdir -p "$SOURCES_DIR"
if ! [ -f "${SOURCES_DIR}/Sources.xz" ]; then
    mkdir -p $SOURCES_DIR
    wget -O "${SOURCES_DIR}/Sources.xz" https://ftp.debian.org/debian/dists/sid/main/source/Sources.xz
fi

CRONS_DIR=/srv/qa.debian.org/data/cronjobs/
if [ -x "${CRONS_DIR}/carnivore" ]; then
    "${CRONS_DIR}"/carnivore
fi

exec "$@"

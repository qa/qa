#! /usr/bin/env python3
#  Copyright (C) 2002 Igor Genibel
#  Copyright (C) 2006 Christoph Berg <myon@debian.org>
#  Copyright (C) 2018 Paul Gevers <elbrus@debian.org>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

import sys, os, re, dbm, ssl
from urllib.request import urlopen
import yaml

def extract_excuses(url, name, type, experimental):
    excuses_db = dbm.open(f"excuses-{name}-new", 'c')
    debian_ca_path = '/etc/ssl/ca-debian'
    if os.path.isdir(debian_ca_path):
        context = ssl.create_default_context(capath=debian_ca_path)
    else:
        context = None

    try:
        with urlopen(url, timeout=10, context=context) as stream:
            ex = yaml.safe_load(stream)
    except OSError as err:
        reason = getattr(err, 'reason', None)
        if not reason:
            reason = getattr(err, 'strerror', None)
        if not reason:
            reason = err
        print('WARNING: skipping %s %s due to: %s' % (name, type, reason), file=sys.stderr)
        return

    excuses_db['_timestamp'] = ex['generated-date'].ctime()

    re_less_eq     = re.compile(r'\(<= ')
    re_less        = re.compile(r'\(<< ')
    re_less_old    = re.compile(r'\(< ')
    re_greater_eq  = re.compile(r'\(>= ')
    re_greater     = re.compile(r'\(>> ')
    re_greater_old = re.compile(r'\(> ')
    re_target = re.compile(r' target="_blank"')
    re_other_pkg   = re.compile(r'href="#')
    if experimental:
        excuses_href = 'href="excuses.php?experimental=1&package='
    else:
        excuses_href = 'href="excuses.php?package='

    for source in ex['sources']:
        try:
            package = source['source']
        except KeyError:
            print('WARNING: skipping %s %s item due to missing source key in:' % (name, type), file=sys.stderr)
            yaml.dump([source], sys.stderr)
            continue

        # Removals, (testing-)proposed-updates and binary only
        if source['item-name'] != package:
            continue

        version = source['old-version']
        # "Excuse for " must not be changed as grep-excuses relies on it
        if experimental:
            excuse = "<!-- Unstable_version: " + version + " -->\n"
            excuse += "<h2>Experimental: Pseudo-Excuse for " + package + "</h2>\n<ul>"
        else:
            excuse = "<!-- Testing_version: " + version + " -->\n"
            excuse += "<h2>Testing: Excuse for " + package + "</h2>\n<ul>"

        indented = False
        for line in source['excuses']:
            stripped_this_line = False
            if line.startswith("∙ ∙ "):
                line = line[4:]
                stripped_this_line = True
            if not indented and stripped_this_line:
                excuse += "<ul>\n"
                indented = True
            elif indented and not stripped_this_line:
                excuse += "</ul>\n"
                indented = False
            line = re_less.sub('(&lt;&lt; ', line)
            line = re_less_eq.sub('(&lt;= ', line)
            line = re_less_old.sub('(&lt; ', line)
            line = re_greater.sub('(&gt;&gt; ', line)
            line = re_greater_eq.sub('(&gt;= ', line)
            line = re_greater_old.sub('(&gt; ', line)
            line = re_target.sub('', line)
            line = re_other_pkg.sub(excuses_href, line)
            excuse += '<li>' + line + '</li>\n'
        if indented:
            excuse += "</ul>\n"

        try:
            for depends in source['dependencies']['blocked-by']:
                excuse += '<li>Depends: ' + package
                excuse += ' <a ' + excuses_href
                excuse +=  depends + '">' + depends
                excuse += '</a> (not considered)</li>\n'
        except KeyError:
            pass

        try:
            for depends in source['dependencies']['migrate-after']:
                if '/' not in depends:
                    excuse += '<li>Depends: ' + package
                    excuse += ' <a ' + excuses_href
                    excuse +=  depends + '">' + depends
                    excuse += '</a></li>\n'
        except KeyError:
            pass

        if source['is-candidate'] == 'false':
            excuse += '<li>Not considered</li>\n'
        excuse += '</ul>'

        excuses_db[package] = excuse

    excuses_db.close()
    return

extract_excuses("https://release.debian.org/britney/excuses.yaml", 'testing', 'excuses', False)
extract_excuses("https://release.debian.org/britney/pseudo-excuses-experimental.yaml", 'experimental', 'pseudo-excuses', True)

# vim:sw=4:softtabstop=4:expandtab

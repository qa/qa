#! /usr/bin/perl -w
#
#
#  Copyright (C) 2004 Andreas Barth
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

use strict;

my (%source, %global);

foreach my $release ('oldstable', 'stable', 'testing', 'unstable') {
    open(SOURCES, "/srv/qa.debian.org/data/ftp/get-packages -s $release -a source |") || die "Cant open sources file: $!";
    my ($package, $binaries) = (undef, undef);
    while(<SOURCES>) {
        chomp;
        $package  = $1 if /^Package: (.+)$/;
        $binaries = [split /\s*,\s*/, $1] if /^Binary: (.+)$/;

        if (/^$/ && $package && $binaries) {
            $source{$_} = $package for (@$binaries);
            ($package, $binaries) = (undef, undef);
        }
    }
    close SOURCES;
    
    if ($package && $binaries) {
        $source{$_} = $package for (@$binaries);
        ($package, $binaries) = (undef, undef);
    }
}
    
open(IN, "< /srv/qa.debian.org/data/bts2ldap/fullindex") || die "Cant open data file: $!";
my $bug = {};
while(<IN>) {
    chomp;
    
    if (/^$/) {
        &add_bug_to_index($bug);
        $bug={};
    } elsif (/^debbugsState: done/) {
        $bug->{'ignore'} = 1;
    } elsif (/^debbugsID: (.+)/) {
        $bug->{'id'} = $1;
    } elsif (/^debbugsPackage: (.+)/) {
        my $pkg = $1;
        if (!$bug->{'packages'}) {
            $bug->{'packages'} = {};
        }
        if ($pkg =~ /^src:(.+)$/) {
            $bug->{'packages'}->{$1} = 1;
        } else {
            $bug->{'packages'}->{$source{$pkg} || $pkg} = 1;
        }
    } elsif (/^debbugsMergedWith: (.+)/) {
        # Mark all duplicates as merged with the oldest one
        next if $1 > $bug->{'id'};
        $bug->{'merged-with'} = $1 if (!$bug->{'merged-with'} || ($bug->{'merged-with'} > $1));
    } elsif (/^debbugsTag: (fixed$|pending$)/) {
        $bug->{'fixed-or-pending'} = "yes";
    } elsif (/^debbugsTag: patch$/) {
        $bug->{'patch'} = "yes";
    } elsif (/^debbugsSeverity: (.+)/) {
        $bug->{'severity'}= $1;
    }
}

&add_bug_to_index($bug);

foreach my $i (keys %global) {
    print "$i:";
    foreach my $j ('RC', 'IN', 'MW', 'FP', 'patch') {
        $global{$i}{$j} ||= "0";
        $global{$i}{$j."M"} ||= "0";
        print $global{$i}{$j."M"}."(".$global{$i}{$j}.")";
        if ($j ne 'patch') {
            print " ";
        }
    }
    print "\n";
}

sub add_bug_to_index {
    my $bug = shift;
    
    if (scalar keys %$bug) {
        my $severity;
        $severity = "RC" if (grep { $bug->{'severity'} eq $_ } qw(critical grave serious));
        $severity = "IN" if (grep { $bug->{'severity'} eq $_ } qw(important normal));
        $severity = "MW" if (grep { $bug->{'severity'} eq $_ } qw(minor wishlist));
        die "Unknown severity $$bug{severity} at package $$bug{packages} ($$bug{id})" unless $severity;
        
        $severity = "FP" if $bug->{'fixed-or-pending'};

        return if $bug->{'ignore'};

        for my $package (keys %{$bug->{packages}}) {
            $global{$package} = {} if ! $global{$package};
            $global{$package}->{$severity}++;

            $global{$package}->{'patch'}++ if $bug->{'patch'} and $severity ne "FP";

            #Count merged bugs once:
            if (!$bug->{'merged-with'}) {
                $global{$package}->{$severity."M"}++; 
                $global{$package}->{'patchM'}++ if $bug->{'patch'} and $severity ne "FP";
            }
        }
    }
}

# vim: sw=4 et

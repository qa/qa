#!/usr/bin/perl -w
# vim:sw=4:

# Copyright 2005, 2006 Christoph Berg <myon@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use strict;
use DB_File;

# Configuration variables
my $descriptions_filename = "descriptions-new.db";
my $tasks_filename = "tasks-new.db";

# read packages files

open G, "bash -c 'xzcat /srv/qa.debian.org/data/ftp/debian/{sid,experimental}/*/binary-*/Packages.xz' |
	grep-dctrl -sPackage -sSource -sDescription -sDepends -sRecommends -d . |" or die "$!";

my ($p, $s, $d, $t, %d, %t);
my ($depends, $recommends, %taskbinpkgs, %bin2src);
while (<G>) {
    chomp;
    if (/^Package: (\S+)/) { $p = $1; }
    if (/^Source: (\S+)/) { $s = $1; }
    if (/^Description: (.+)/) { $d = $1; }
    if (/^Depends: (.+)/) { $depends = $1; }
    if (/^Recommends: (.+)/) { $recommends = $1; }

    if (/^$/ and $p) {
	# Description
	$d{"d:$p"} = $d if $d;
    }

    if (/^$/ and $p and $s and $s eq "tasksel" and $p =~ /^task-(.*)$/ )
    {
	# Task
        $t = $1;

        my $binpkgs = "";
        $binpkgs .= " $depends " if( $depends );
        $binpkgs .= " $recommends " if( $recommends );
        $binpkgs =~ s/[,|]/ /g;
        $binpkgs =~ s/ +tasksel *(?:\(= *(?:[0-9]+:)?[-.+~A-Za-z0-9]+(?:-[.+~A-Za-z0-9]+)?\))? */ /g; # remove "tasksel (= 3.21)"
        $binpkgs =~ s/  */ /g;
        $binpkgs =~ s/^ *(.*?) *$/$1/;

        if( $binpkgs !~ /^[a-z0-9 \-\.\+]*$/ )
        {
            print STDERR "special characters in packages of task $t (binpkgs=$binpkgs)\n";
            $binpkgs = "";
        }

        foreach my $binpkg ( split / /, $binpkgs )
        {
            $taskbinpkgs{$t}->{$binpkg} = 1;
        }
    }

    if (/^$/ and $p)
    {
        $s = $p if( ! $s );
        # binary packages from source packages
        $bin2src{$p}->{$s} = 1;
    }

    if (/^$/) {
	undef $p; undef $s; undef $d; undef $t;
        undef $depends; undef $recommends;
    }
}

close G;

# deal with task-desktop pointing to tasks pointing back to task-desktop

foreach $p ( keys %{$taskbinpkgs{'desktop'}} )
{
    delete $taskbinpkgs{'desktop'}->{$p}
        if( $p =~ /^task-(.*)$/ and defined $taskbinpkgs{$1}->{'task-desktop'} );
}

foreach $t ( keys %taskbinpkgs )
{
    if( defined $taskbinpkgs{$t}->{'task-desktop'} )
    {
        foreach my $desktopbinpkg ( keys %{$taskbinpkgs{'desktop'}} )
        {
            $taskbinpkgs{$t}->{$desktopbinpkg} = 1;
        }
        delete $taskbinpkgs{$t}->{'task-desktop'};
    }
}

# check for remaining tasks pointing to other tasks

foreach $t ( keys %taskbinpkgs )
{
    foreach $p ( keys %{$taskbinpkgs{$t}} )
    {
        if( $p =~ /^task-(.*)$/ and defined $bin2src{$p}->{"tasksel"} )
        {
            print STDERR "task $t points to package $p which is task $1\n";
        }
    }
}

# conversion from task binary packages to task source packages

foreach $t ( keys %taskbinpkgs )
{
    foreach $p ( keys %{$taskbinpkgs{$t}} )
    {
        foreach $s ( keys %{$bin2src{$p}} )
        {
            $t{$t}->{$s} = 1;
        }
    }
}

# write out results

my %descriptions;
my $descriptions_db = tie %descriptions, "DB_File", $descriptions_filename,
	O_RDWR|O_CREAT, 0666, $DB_BTREE
	or die "Can't open database $descriptions_filename : $!";

%descriptions = %d;

my %tasks;
my $tasks_db = tie %tasks, "DB_File", $tasks_filename,
	O_RDWR|O_CREAT, 0666, $DB_BTREE
	or die "Can't open database $tasks_filename : $!";

foreach (keys %t) {
    $tasks{"t:$_"} = join ' ', keys %{$t{$_}};
}

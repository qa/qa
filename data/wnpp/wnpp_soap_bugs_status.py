from urllib.error import HTTPError, URLError
from urllib.request import Request, urlopen

from lxml import etree


class SoapBugsStatus:
    """Class to handle get_bugs and get_status service."""

    def __init__(self, wsld_url: str, soap_action: str | None = None) -> None:
        """Initialize the SoapBugsStatus class.

        Args:
            wsld_url (str): WSLD url.
            soap_action (str | None, optional): Soap action to use. Defaults to None.
        """
        self.wsld_url = wsld_url
        self.headers = {
            "Content-Type": "text/xml; charset=utf-8",
        }
        if soap_action and isinstance(soap_action, str):
            self.headers["SOAPAction"] = soap_action

    def get_response_from_soap_body(
        self, soap_body: str, remove_declaration: bool = True
    ) -> str:
        """Connect the SoapBugsStatus class to the WSLD server and receive related response from soap_body input.

        Args:
            soap_body (str): Soap body to send.
            remove_declaration (bool): Remove declaration if True, False otherwise. Defaults to True.

        Returns:
            str: SOAP response.
        """
        req = Request(
            self.wsld_url, data=soap_body.encode("utf-8"), headers=self.headers
        )
        try:
            with urlopen(req) as response:
                response_data = response.read().decode("utf-8")
        except HTTPError as error:
            error_msg = f"HTTPError: {error.code} {error.reason}\nError Response:\n{error.read().decode('utf-8')}"
            raise Exception(error_msg)
        except URLError as error:
            error_msg = f"URLError: {error.reason}"
            raise Exception(error_msg)
        return (
            response_data.replace('<?xml version="1.0" encoding="UTF-8"?>', "")
            if remove_declaration
            else response_data
        )

    def get_bugs(self, *item_list: tuple[str]) -> list:
        """Get a list of bugs.

        Args:
            *item_list (tuple[str]): item list to get bugs.

        Returns:
            list: List of bugs.
        """
        soap_body = """<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Debbugs/SOAP">
           <soapenv:Header/>
           <soapenv:Body>
              <urn:get_bugs>
        """
        for item in item_list:
            soap_body += f"        <item>{item}</item>"
        soap_body += """      </urn:get_bugs>
           </soapenv:Body>
        </soapenv:Envelope>"""
        response_xml = self.get_response_from_soap_body(soap_body, True)
        items = []
        if response_xml:
            dom = etree.fromstring(response_xml)
            namespaces = {
                "soap": "http://schemas.xmlsoap.org/soap/envelope/",
                "soapenc": "http://schemas.xmlsoap.org/soap/encoding/",
                "xsd": "http://www.w3.org/2001/XMLSchema",
                "xsi": "http://www.w3.org/2001/XMLSchema-instance",
                "a": "urn:Debbugs/SOAP",
            }
            for item in dom.findall(".//a:item", namespaces):
                items.append(item.text)
        return items

    def get_status(self, bug_numbers: list[str | int]) -> dict:
        """Get the status of the bugs.

        Args:
            bug_numbers (list[str | int]): List of bug numbers.:

        Returns:
            dict: Dictionary of bug statuses.
        """
        soap_body = """<?xml version="1.0" encoding="UTF-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:tns="urn:Debbugs/SOAP">
          <soap:Body>
            <tns:get_status>
              <bugs soapenc:arrayType="xsd:int[{0}]" xsi:type="soapenc:Array">
        """.format(
            len(bug_numbers)
        )
        for number in bug_numbers:
            soap_body += f'        <item xsi:type="xsd:int">{number}</item>\n'
        soap_body += """      </bugs>
            </tns:get_status>
          </soap:Body>
        </soap:Envelope>"""
        response_xml = self.get_response_from_soap_body(soap_body, True)
        dom = etree.fromstring(response_xml)
        xml_as_dict = self.xml_to_dict(dom)
        return xml_as_dict

    @staticmethod
    def clean_tag_key(tag: str) -> str:
        """Clean the tag key from all tags.

        Args:
            tag (str): The tag to clean.

        Returns:
            str: The cleaned tag.
        """
        return tag.split("}")[-1] if "}" in tag else tag

    def xml_to_dict(self, element: etree._Element) -> dict:
        """Convert the xml element to dict.

        Args:
            element (etree._Element): The xml element to convert.

        Returns:
            dict: The converted xml element.
        """
        if list(element):
            children = {}
            for child in element:
                child_tag = self.clean_tag_key(child.tag)
                child_dict = self.xml_to_dict(child)

                if child_tag in children:
                    if isinstance(children[child_tag], list) and child_tag in children:
                        children[child_tag].append(child_dict[child_tag])
                    else:
                        children[child_tag] = [
                            children[child_tag],
                            child_dict[child_tag],
                        ]
                else:
                    children[child_tag] = child_dict[child_tag]

            if element.text and element.text.strip():
                children["text"] = element.text.strip()

            return {self.clean_tag_key(element.tag): children}
        else:
            return {
                self.clean_tag_key(element.tag): (
                    element.text.strip() if element.text else None
                )
            }

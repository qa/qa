#!/usr/bin/python3

# Show orphaned packages for which no WNPP bugs have been filed yet
# Copyright (C) 2001, 2002, 2004  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import string
import wnpp
import sys
sys.path.append("/srv/ftp.debian.org/dak")
import pg, db_access

Cnf = None
projectB = pg.connect("projectb")
db_access.init(Cnf, projectB)

# Get a listing of those orphaned source packages for which a WNPP bug
# has already been filed. (Problem: If wnpp.proper_subject cannot
# parse the bug's subject line, the script will think there is no bug
# although there is.)
bug_exists = []
ldap_result = wnpp.query(["debbugsTitle"])
if not ldap_result:
    print("Problem with LDAP search")
    sys.exit(1)
else:
    for _, dict in ldap_result:
        if not dict:
            continue
        subject = dict["debbugsTitle"][0]
        result = wnpp.weak_subject.search(subject)
        if result:
            tag = result.group(1)
            package = string.lower(result.group(2))
        else:
            continue

        if tag in wnpp.exists:
            bug_exists.append(package)

# Get a listing of all source packages plus their maintainers
suite_id = db_access.get_suite_id("unstable")
q = projectB.query("SELECT s.source, m.name FROM source s, src_associations sa, maintainer m WHERE sa.suite = %d AND sa.source = s.id AND s.maintainer = m.id" % suite_id)

for (source, maintainer) in q.getresult():
    # Check for orphaned packages (i.e. maintained by QA)
    if (string.count(maintainer, "debian-qa@lists.") or
       string.count(maintainer, "packages@qa.")):
           if source not in bug_exists:
               # No bug has been filed yet
               print(source)

# vim: ts=4:expandtab:shiftwidth=4:
